package com.eightam.android.multilevelmenu;

import com.eightam.android.multilevelmenu.model.Menu;
import com.eightam.android.multilevelmenu.model.MenuItem;

import junit.framework.TestCase;

import java.util.Stack;

public class ModelTestCase extends TestCase {

    Menu m;
    MenuItem a;
    MenuItem b;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        m = new Menu(
                new MenuItem("topmost-1", "Topmost 1", null),
                new MenuItem("topmost-2", "Topmost 2", null,
                        new MenuItem("child-2.1", "Child 2.1", null),
                        new MenuItem("child-2.2", "Child 2.2", null,
                                new MenuItem("child-2.2.1", "Child 2.2.1", null),
                                new MenuItem("child-2.2.2", "Child 2.2.2", null),
                                new MenuItem("child-2.2.3", "Child 2.2.3", null),
                                new MenuItem("child-2.2.4", "Child 2.2.4", null,
                                        new MenuItem("child-2.2.4.1", "Child 2.2.4.1", null),
                                        new MenuItem("child-2.2.4.2", "Child 2.2.4.2", null),
                                        new MenuItem("child-2.2.4.3", "Child 2.2.4.3", null)
                                )
                        )
                ),
                new MenuItem("topmost-3", "Topmost 3", null,
                        new MenuItem("child-3.1", "Child 3.1", null),
                        new MenuItem("child-3.2", "Child 3.2", null,
                                new MenuItem("child-3.2.1", "Child 3.2.1", null),
                                new MenuItem("child-3.2.2", "Child 3.2.2", null),
                                new MenuItem("child-3.2.3", "Child 3.2.3", null)
                        ),
                        new MenuItem("child-3.3", "Child 3.3", null)
                ),
                new MenuItem("topmost-4", "Topmost 4", null,
                        new MenuItem("child-4.1", "Child 4.1", null),
                        new MenuItem("child-4.2", "Child 4.2", null)
                )
        );

        a = new MenuItem("root", "Root", null,
                new MenuItem("child-1", "Child 1", null),
                new MenuItem("child-2", "Child 2", null,
                        new MenuItem("child-2.1", "Child 2.1", null),
                        new MenuItem("child-2.2", "Child 2.2", null)
                ),
                new MenuItem("child-3", "Child 3", null)
        );

        b = new MenuItem("root", "Root", null,
                new MenuItem("child-1", "Child 1", null),
                new MenuItem("child-2", "Child 2", null,
                        new MenuItem("child-2.1", "Child 2.1", null),
                        new MenuItem("child-2.2", "Child 2.2", null)
                ),
                new MenuItem("child-3", "Child 3", null)
        );
    }

    public void testMenuItemEquality() {
        assertTrue("a is not equal to b.", a.equals(b));
    }

    public void testParsingPath() {
        String path1 = "/topmost-1";
        String path2 = "/topmost-2/child-2.2/child-2.2.4/child-2.2.4.2";
        String path3 = "/topmost-3/child-3.1";
        String path4 = "/topmost-3/child-3.2/child-3.2.3";
        String path5 = "/this/is/an/invalid/path";
        String path6 = "/this/is/an/invalid/path/";
        String path7 = "/";
        String path8 = "path";
        String path9 = "topmost-1";

        MenuItem menuItem1 = m.findMenuItemByPath(path1);
        MenuItem menuItem2 = m.findMenuItemByPath(path2);
        MenuItem menuItem3 = m.findMenuItemByPath(path3);
        MenuItem menuItem4 = m.findMenuItemByPath(path4);
        MenuItem menuItem5 = m.findMenuItemByPath(path5);
        MenuItem menuItem6 = m.findMenuItemByPath(path6);
        MenuItem menuItem7 = m.findMenuItemByPath(path7);
        MenuItem menuItem8 = m.findMenuItemByPath(path8);
        MenuItem menuItem9 = m.findMenuItemByPath(path9);

        assertNotNull("Menu item 1 is null.", menuItem1);
        assertNotNull("Menu item 2 is null.", menuItem2);
        assertNotNull("Menu item 3 is null.", menuItem3);
        assertNotNull("Menu item 4 is null.", menuItem4);

        assertTrue("Menu item 1 id is " + menuItem1.getId(), menuItem1.getId().equals("topmost-1"));
        assertTrue("Menu item 2 id is " + menuItem2.getId(), menuItem2.getId().equals("child-2.2.4.2"));
        assertTrue("Menu item 3 id is " + menuItem3.getId(), menuItem3.getId().equals("child-3.1"));
        assertTrue("Menu item 4 id is " + menuItem4.getId(), menuItem4.getId().equals("child-3.2.3"));

        assertNull("Menu item 5 is not null.", menuItem5);
        assertNull("Menu item 6 is not null.", menuItem6);
        assertNull("Menu item 7 is not null.", menuItem7);
        assertNull("Menu item 8 is not null.", menuItem8);
        assertNull("Menu item 9 is not null.", menuItem9);
    }

    public void testMenuNavigationByMenuItem() {
        String path1 = "/topmost-1";
        String path2 = "/topmost-2/child-2.2/child-2.2.4/child-2.2.4.2";
        String path3 = "/topmost-3/child-3.1";
        String path4 = "/topmost-3/child-3.2/child-3.2.3";
        String navigationPath;

        MenuItem menuItem1 = m.getMenuItemAt(0);
        MenuItem menuItem2 = m.getMenuItemAt(1).getChildAt(1).getChildAt(3).getChildAt(1);
        MenuItem menuItem3 = m.getMenuItemAt(2).getChildAt(0);
        MenuItem menuItem4 = m.getMenuItemAt(2).getChildAt(1).getChildAt(2);

        m.navigateTo(menuItem1);
        navigationPath = m.getNavigationPath();

        assertTrue("[Path 1] Navigation path is " + navigationPath + ", it should be " + path1,
                path1.equals(navigationPath));

        m.navigateTo(menuItem2);
        navigationPath = m.getNavigationPath();

        assertTrue("[Path 2] Navigation path is " + navigationPath + ", it should be " + path2,
                path2.equals(navigationPath));

        m.navigateTo(menuItem3);
        navigationPath = m.getNavigationPath();

        assertTrue("[Path 3] Navigation path is " + navigationPath + ", it should be " + path3,
                path3.equals(navigationPath));

        m.navigateTo(menuItem4);
        navigationPath = m.getNavigationPath();

        assertTrue("[Path 4] Navigation path is " + navigationPath + ", it should be " + path4,
                path4.equals(navigationPath));
    }

    public void testMenuNavigationByPath() {
        String path1 = "/topmost-1";
        String topOfNavigationStack1 = "topmost-1";
        String path2 = "/topmost-2/child-2.2/child-2.2.4/child-2.2.4.2";
        String topOfNavigationStack2 = "child-2.2.4.2";
        String path3 = "/topmost-3/child-3.1";
        String topOfNavigationStack3 = "child-3.1";
        String path4 = "/topmost-3/child-3.2/child-3.2.3";
        String topOfNavigationStack4 = "child-3.2.3";

        String navigationPath;
        Stack<MenuItem> navigationStack;

        m.navigateTo(path1);
        navigationPath = m.getNavigationPath();
        navigationStack = m.getNavigationStack();

        assertTrue("[Path 1] Navigation path is " + navigationPath + ", it should be " + path1,
                path1.equals(navigationPath));

        assertTrue("[Path 1] Navigation stack is empty.", !navigationStack.isEmpty());

        assertTrue("[Path 1] Menu item on top of the navigation stack is " + navigationStack.peek().getId() +
                ", it should be " + topOfNavigationStack1, navigationStack.peek().getId().equals(topOfNavigationStack1));

        m.navigateTo(path2);
        navigationPath = m.getNavigationPath();
        navigationStack = m.getNavigationStack();

        assertTrue("[Path 2] Navigation path is " + navigationPath + ", it should be " + path2,
                path2.equals(navigationPath));

        assertTrue("[Path 2] Navigation stack is empty.", !navigationStack.isEmpty());

        assertTrue("[Path 2] Menu item on top of the navigation stack is " + navigationStack.peek().getId() +
                ", it should be " + topOfNavigationStack2, navigationStack.peek().getId().equals(topOfNavigationStack2));

        m.navigateTo(path3);
        navigationPath = m.getNavigationPath();
        navigationStack = m.getNavigationStack();

        assertTrue("[Path 3] Navigation path is " + navigationPath + ", it should be " + path3,
                path3.equals(navigationPath));

        assertTrue("[Path 3] Navigation stack is empty.", !navigationStack.isEmpty());

        assertTrue("[Path 3] Menu item on top of the navigation stack is " + navigationStack.peek().getId() +
                ", it should be " + topOfNavigationStack3, navigationStack.peek().getId().equals(topOfNavigationStack3));

        m.navigateTo(path4);
        navigationPath = m.getNavigationPath();
        navigationStack = m.getNavigationStack();

        assertTrue("[Path 4] Navigation path is " + navigationPath + ", it should be " + path4,
                path4.equals(navigationPath));

        assertTrue("[Path 4] Navigation stack is empty.", !navigationStack.isEmpty());

        assertTrue("[Path 4] Menu item on top of the navigation stack is " + navigationStack.peek().getId() +
                ", it should be " + topOfNavigationStack4, navigationStack.peek().getId().equals(topOfNavigationStack4));
    }

    public void testMenuSelectionByMenuItem() {
        String path1 = "/topmost-1";
        String path2 = "/topmost-2/child-2.2/child-2.2.4/child-2.2.4.2";
        String path3 = "/topmost-3/child-3.1";
        String path4 = "/topmost-3/child-3.2/child-3.2.3";
        String path5 = "/topmost-4";
        String selectionPath;

        MenuItem menuItem1 = m.getMenuItemAt(0);
        MenuItem menuItem2 = m.getMenuItemAt(1).getChildAt(1).getChildAt(3).getChildAt(1);
        MenuItem menuItem3 = m.getMenuItemAt(2).getChildAt(0);
        MenuItem menuItem4 = m.getMenuItemAt(2).getChildAt(1).getChildAt(2);
        MenuItem menuItem5 = m.getMenuItemAt(3);

        m.navigateTo(menuItem1);
        selectionPath = m.getSelectionPath();

        assertTrue("[Path 1] Selection path is " + selectionPath + ", it should be " + path1,
                path1.equals(selectionPath));

        m.navigateTo(menuItem2);
        selectionPath = m.getSelectionPath();

        assertTrue("[Path 2] Selection path is " + selectionPath + ", it should be " + path2,
                path2.equals(selectionPath));

        m.navigateTo(menuItem3);
        selectionPath = m.getSelectionPath();

        assertTrue("[Path 3] Selection path is " + selectionPath + ", it should be " + path3,
                path3.equals(selectionPath));

        m.navigateTo(menuItem4);
        selectionPath = m.getSelectionPath();

        assertTrue("[Path 4] Selection path is " + selectionPath + ", it should be " + path4,
                path4.equals(selectionPath));

        m.navigateTo(menuItem5);
        selectionPath = m.getSelectionPath();

        assertTrue("[Path 5] Selection path is " + selectionPath + ", it should be " + path4,
                path4.equals(selectionPath));
        assertFalse("[Path 5] Selection path is " + selectionPath + ", it should be " + path4,
                path5.equals(selectionPath));
    }

    public void testMenuSelectionByPath() {
        String path1 = "/topmost-1";
        String topOfNavigationStack1 = "topmost-1";
        String path2 = "/topmost-2/child-2.2/child-2.2.4/child-2.2.4.2";
        String topOfNavigationStack2 = "child-2.2.4.2";
        String path3 = "/topmost-3/child-3.1";
        String topOfNavigationStack3 = "child-3.1";
        String path4 = "/topmost-3/child-3.2/child-3.2.3";
        String topOfNavigationStack4 = "child-3.2.3";
        String path5 = "/topmost-4";

        String selectionPath;
        Stack<MenuItem> selectionStack;

        m.navigateTo(path1);
        selectionPath = m.getSelectionPath();
        selectionStack = m.getSelectionStack();

        assertTrue("[Path 1] Selection path is " + selectionPath + ", it should be " + path1,
                path1.equals(selectionPath));

        assertTrue("[Path 1] Selection stack is empty.", !selectionStack.isEmpty());

        assertTrue("[Path 1] Menu item on top of the selection stack is " + selectionStack.peek().getId() +
                ", it should be " + topOfNavigationStack1, selectionStack.peek().getId().equals(topOfNavigationStack1));

        m.navigateTo(path2);
        selectionPath = m.getSelectionPath();
        selectionStack = m.getSelectionStack();

        assertTrue("[Path 2] Selection path is " + selectionPath + ", it should be " + path2,
                path2.equals(selectionPath));

        assertTrue("[Path 2] Selection stack is empty.", !selectionStack.isEmpty());

        assertTrue("[Path 2] Menu item on top of the selection stack is " + selectionStack.peek().getId() +
                ", it should be " + topOfNavigationStack2, selectionStack.peek().getId().equals(topOfNavigationStack2));

        m.navigateTo(path3);
        selectionPath = m.getSelectionPath();
        selectionStack = m.getSelectionStack();

        assertTrue("[Path 3] Selection path is " + selectionPath + ", it should be " + path3,
                path3.equals(selectionPath));

        assertTrue("[Path 3] Selection stack is empty.", !selectionStack.isEmpty());

        assertTrue("[Path 3] Menu item on top of the selection stack is " + selectionStack.peek().getId() +
                ", it should be " + topOfNavigationStack3, selectionStack.peek().getId().equals(topOfNavigationStack3));

        m.navigateTo(path4);
        selectionPath = m.getSelectionPath();
        selectionStack = m.getSelectionStack();

        assertTrue("[Path 4] Selection path is " + selectionPath + ", it should be " + path4,
                path4.equals(selectionPath));

        assertTrue("[Path 4] Selection stack is empty.", !selectionStack.isEmpty());

        assertTrue("[Path 4] Menu item on top of the selection stack is " + selectionStack.peek().getId() +
                ", it should be " + topOfNavigationStack4, selectionStack.peek().getId().equals(topOfNavigationStack4));

        m.navigateTo(path5);
        selectionPath = m.getSelectionPath();
        selectionStack = m.getSelectionStack();

        assertTrue("[Path 5] Selection path is " + selectionPath + ", it should be " + path4,
                path4.equals(selectionPath));

        assertTrue("[Path 5] Selection stack is empty.", !selectionStack.isEmpty());

        assertTrue("[Path 5] Menu item on top of the selection stack is " + selectionStack.peek().getId() +
                ", it should be " + topOfNavigationStack4, selectionStack.peek().getId().equals(topOfNavigationStack4));
    }

    public void testBackNavigation() {
        String path = "/topmost-2/child-2.2/child-2.2.4/child-2.2.4.2";
        String sub1 = "child-2.2.4";
        String sub2 = "child-2.2";
        String sub3 = "topmost-2";

        MenuItem topOfNavigationStack;

        m.navigateTo(path);
        assertTrue("Not possible to go back, it should be possible.", m.canGoBack());

        m.navigateBack();
        topOfNavigationStack = !m.getNavigationStack().isEmpty() ? m.getNavigationStack().peek() : null;

        assertNotNull("Menu item on top of the selection stack is null, it should not be.", topOfNavigationStack);
        assertTrue("Menu item on top of the selection stack is " + topOfNavigationStack.getId() + ", it should be " + sub1,
                topOfNavigationStack.getId().equals(sub1));
        assertTrue("Not possible to go back, it should be possible.", m.canGoBack());

        m.navigateBack();
        topOfNavigationStack = !m.getNavigationStack().isEmpty() ? m.getNavigationStack().peek() : null;

        assertNotNull("Menu item on top of the selection stack is null, it should not be.", topOfNavigationStack);
        assertTrue("Menu item on top of the selection stack is " + topOfNavigationStack.getId() + ", it should be " + sub2,
                topOfNavigationStack.getId().equals(sub2));
        assertTrue("Not possible to go back, it should be possible.", m.canGoBack());

        m.navigateBack();
        topOfNavigationStack = !m.getNavigationStack().isEmpty() ? m.getNavigationStack().peek() : null;

        assertNotNull("Menu item on top of the selection stack is null, it should not be.", topOfNavigationStack);
        assertTrue("Menu item on top of the selection stack is " + topOfNavigationStack.getId() + ", it should be " + sub3,
                topOfNavigationStack.getId().equals(sub3));
        assertTrue("Not possible to go back, it should be possible.", m.canGoBack());

        m.navigateBack();
        topOfNavigationStack = !m.getNavigationStack().isEmpty() ? m.getNavigationStack().peek() : null;

        assertNull("Menu item on top of the selection stack is not null, it should be.", topOfNavigationStack);
        assertFalse("Possible to go back, it should not be possible.", m.canGoBack());
    }

}
