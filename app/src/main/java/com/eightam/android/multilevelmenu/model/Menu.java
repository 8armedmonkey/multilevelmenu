package com.eightam.android.multilevelmenu.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Menu {

    public static final String DEFAULT_PATH_SEPARATOR = "/";

    protected List<MenuItem> mMenuItems = new ArrayList<>();
    protected Stack<MenuItem> mNavigationStack = new Stack<>();
    protected Stack<MenuItem> mSelectionStack = new Stack<>();
    protected String mPathSeparator = DEFAULT_PATH_SEPARATOR;
    protected Object mMenuItemsLock = new Object();

    public Menu() {
    }

    public Menu(MenuItem... menuItems) {
        if (menuItems != null) {
            for (MenuItem menuItem : menuItems) {
                if (menuItem != null) {
                    mMenuItems.add(menuItem);
                }
            }
        }
    }

    public List<MenuItem> getMenuItems() {
        synchronized (mMenuItemsLock) {
            return new ArrayList<>(mMenuItems);
        }
    }

    public int getMenuItemCount() {
        synchronized (mMenuItemsLock) {
            return mMenuItems.size();
        }
    }

    public MenuItem getMenuItemAt(int position) {
        synchronized (mMenuItemsLock) {
            if (position >= 0 && position < getMenuItemCount()) {
                return mMenuItems.get(position);
            }
            return null;
        }
    }

    public void addMenuItem(MenuItem menuItem) {
        synchronized (mMenuItemsLock) {
            mMenuItems.add(menuItem);
        }
    }

    public void addMenuItem(MenuItem menuItem, int position) {
        synchronized (mMenuItemsLock) {
            if (position >= 0 && position < getMenuItemCount()) {
                mMenuItems.add(position, menuItem);
            }
        }
    }

    public void removeMenuItem(MenuItem menuItem) {
        synchronized (mMenuItemsLock) {
            mMenuItems.remove(menuItem);
        }
    }

    public void removeMenuItem(int position) {
        synchronized (mMenuItemsLock) {
            if (position >= 0 && position < getMenuItemCount()) {
                mMenuItems.remove(position);
            }
        }
    }

    public void clearMenuItems() {
        synchronized (mMenuItemsLock) {
            mMenuItems.clear();
        }
    }

    public Stack<MenuItem> getNavigationStack() {
        Stack<MenuItem> stack = new Stack<>();
        stack.addAll(mNavigationStack);

        return stack;
    }

    public String getNavigationPath() {
        Stack<MenuItem> stack = new Stack<>();
        stack.addAll(mNavigationStack);

        return buildPath(stack);
    }

    public Stack<MenuItem> getSelectionStack() {
        Stack<MenuItem> stack = new Stack<>();
        stack.addAll(mSelectionStack);

        return stack;
    }

    public String getSelectionPath() {
        Stack<MenuItem> stack = new Stack<>();
        stack.addAll(mSelectionStack);

        return buildPath(stack);
    }

    public String getPathSeparator() {
        return mPathSeparator;
    }

    public void setPathSeparator(String pathSeparator) {
        mPathSeparator = pathSeparator;
    }

    public void navigateTo(MenuItem menuItem) {
        Stack<MenuItem> navigationStack = new Stack<>();
        buildStack(menuItem, navigationStack);

        mNavigationStack = navigationStack;

        if (menuItem.getChildCount() == 0) {
            Stack<MenuItem> selectionStack = new Stack<>();
            selectionStack.addAll(navigationStack);

            mSelectionStack = selectionStack;
        }
    }

    public void navigateTo(String path) {
        Stack<MenuItem> navigationStack = new Stack<>();
        buildStack(path, null, navigationStack);

        mNavigationStack = navigationStack;

        if (!navigationStack.isEmpty() && navigationStack.peek().getChildCount() == 0) {
            Stack<MenuItem> selectionStack = new Stack<>();
            selectionStack.addAll(navigationStack);

            mSelectionStack = selectionStack;
        }
    }

    public void navigateBack() {
        if (!mNavigationStack.isEmpty()) {
            MenuItem menuItem = mNavigationStack.pop();

            if (!mNavigationStack.isEmpty()) {
                navigateTo(mNavigationStack.peek());
            }
        }
    }

    public boolean canGoBack() {
        return !mNavigationStack.isEmpty();
    }

    public MenuItem findMenuItemByPath(String path) {
        return findMenuItemByPath(path, null);
    }

    private MenuItem findMenuItemByPath(String path, MenuItem parent) {
        int index = path.indexOf(mPathSeparator);

        if (index == -1) {
            if (parent != null) {
                for (MenuItem menuItem : parent.mChildren) {
                    if (menuItem.mId.equals(path)) {
                        return menuItem;
                    }
                }
            }
        } else {
            MenuItem nextParent = null;

            int nextIndex = path.indexOf(mPathSeparator, index + 1);
            String id = nextIndex == -1 ? path.substring(index + 1) : path.substring(index + 1, nextIndex);

            if (parent != null) {
                nextParent = parent.findChildWithId(id);
            } else {
                for (MenuItem menuItem : mMenuItems) {
                    if (menuItem.mId.equals(id)) {
                        nextParent = menuItem;
                        break;
                    }
                }
            }

            if (nextIndex == -1) {
                return nextParent;

            } else {
                return findMenuItemByPath(path.substring(index + 1), nextParent);
            }
        }

        return null;
    }

    private void buildStack(MenuItem menuItem, Stack<MenuItem> stack) {
        if (menuItem.mParent == null) {
            if (mMenuItems.contains(menuItem)) {
                stack.push(menuItem);
            }
        } else {
            buildStack(menuItem.mParent, stack);

            if (stack.peek() != null) {
                stack.push(menuItem);
            }
        }
    }

    private void buildStack(String path, MenuItem parent, Stack<MenuItem> stack) {
        int index = path.indexOf(mPathSeparator);

        if (index == -1) {
            if (parent != null) {
                MenuItem menuItem = parent.findChildWithId(path);

                if (menuItem != null) {
                    stack.push(menuItem);
                }
            }
        } else {
            MenuItem nextParent = null;

            int nextIndex = path.indexOf(mPathSeparator, index + 1);
            String id = nextIndex == -1 ? path.substring(index + 1) : path.substring(index + 1, nextIndex);

            if (parent != null) {
                MenuItem menuItem = parent.findChildWithId(id);

                if (menuItem != null) {
                    nextParent = menuItem;
                    stack.push(menuItem);
                }
            } else {
                for (MenuItem menuItem : mMenuItems) {
                    if (menuItem.mId.equals(id)) {
                        nextParent = menuItem;
                        stack.push(menuItem);
                        break;
                    }
                }
            }

            if (nextIndex != -1) {
                buildStack(path.substring(nextIndex), nextParent, stack);
            }
        }
    }

    private String buildPath(Stack<MenuItem> stack) {
        StringBuilder sb = new StringBuilder();

        while (!stack.isEmpty()) {
            sb.insert(0, stack.pop().mId).insert(0, mPathSeparator);
        }
        return sb.toString();
    }

}
