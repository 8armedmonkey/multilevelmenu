package com.eightam.android.multilevelmenu.model;

import java.util.ArrayList;
import java.util.List;

public class MenuItem {

    protected String mId;
    protected String mTitle;
    protected MenuItem mParent;
    protected List<MenuItem> mChildren = new ArrayList<>();
    protected Object mChildrenLock = new Object();

    public MenuItem(String id, String title, MenuItem parent) {
        this(id, title, parent, new MenuItem[0]);
    }

    public MenuItem(String id, String title, MenuItem parent, MenuItem... children) {
        mId = id;
        mTitle = title;
        mParent = parent;

        if (children != null) {
            for (MenuItem child : children) {
                if (child != null) {
                    child.mParent = MenuItem.this;
                    mChildren.add(child);
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (MenuItem.this == o) {
            return true;
        }

        if (!(o instanceof MenuItem)) {
            return false;
        }

        MenuItem lhs = (MenuItem) o;
        boolean equalsParent = mParent == null && lhs.mParent == null
                || (mParent != null && lhs.mParent != null && mParent.hashCode() == lhs.mParent.hashCode());

        return mId.equals(lhs.mId)
                && mTitle.equals(lhs.mTitle)
                && equalsParent;
    }

    @Override
    public int hashCode() {
        int result = 17;

        if (mId != null) {
            result = 31 * result + mId.hashCode();
        }

        if (mTitle != null) {
            result = 31 * result + mTitle.hashCode();
        }

        if (mParent != null) {
            result = 31 * result + mParent.hashCode();
        }
        return result;
    }

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public MenuItem getParent() {
        return mParent;
    }

    public void setParent(MenuItem parent) {
        mParent = parent;
    }

    public List<MenuItem> getChildren() {
        synchronized (mChildrenLock) {
            return new ArrayList<>(mChildren);
        }
    }

    public int getChildCount() {
        synchronized (mChildrenLock) {
            return mChildren.size();
        }
    }

    public MenuItem getChildAt(int position) {
        synchronized (mChildrenLock) {
            if (position >= 0 && position < getChildCount()) {
                return mChildren.get(position);
            }
            return null;
        }
    }

    public MenuItem findChildWithId(String id) {
        synchronized (mChildrenLock) {
            for (MenuItem menuItem : mChildren) {
                if (menuItem.mId.equals(id)) {
                    return menuItem;
                }
            }
            return null;
        }
    }

    public void addChild(MenuItem menuItem) {
        synchronized (mChildrenLock) {
            mChildren.add(menuItem);
        }
    }

    public void addChild(MenuItem menuItem, int position) {
        synchronized (mChildrenLock) {
            if (position >= 0 && position < getChildCount()) {
                mChildren.add(position, menuItem);
            }
        }
    }

    public void removeChild(MenuItem menuItem) {
        synchronized (mChildrenLock) {
            mChildren.remove(menuItem);
        }
    }

    public void removeChild(int position) {
        synchronized (mChildrenLock) {
            if (position >= 0 && position < getChildCount()) {
                mChildren.remove(position);
            }
        }
    }

    public void clearChildren() {
        synchronized (mChildrenLock) {
            mChildren.clear();
        }
    }

}
